#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """30373
25512
65332
33549
35390"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 21)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 8)
		pass

def parse_input(i):
	grid = []
	for line in i:
		grid.append(list(int(c) for c in line.strip()))
	return grid

def part1(i):
	vis = []
	for _ in range(len(i)):
		vis.append([])
		for _ in range(len(i[0])):
			vis[-1].append(False)
	for y in range(len(i)):
		for x in range(len(i[0])):
			curr = i[y][x]
			acc = True
			for qy in range(0, y):
				if i[qy][x] >= curr:
					acc = False
			if acc:
				vis[y][x] = True
				continue
			acc = True
			for qy in range(y + 1, len(i)):
				if i[qy][x] >= curr:
					acc = False
			if acc:
				vis[y][x] = True
				continue
			acc = True
			for qx in range(0, x):
				if i[y][qx] >= curr:
					acc = False
			if acc:
				vis[y][x] = True
				continue
			acc = True
			for qx in range(x + 1, len(i[0])):
				if i[y][qx] >= curr:
					acc = False
			if acc:
				vis[y][x] = True
				continue
	o = 0
	for y in range(len(i)):
		for x in range(len(i[0])):
			o += (1 if vis[y][x] else 0)
	return o

def part2(i):
	sscore = []
	for _ in range(len(i)):
		sscore.append([])
		for _ in range(len(i[0])):
			sscore[-1].append(0)
	for y in range(len(i)):
		for x in range(len(i[0])):
			curr = i[y][x]
			top_vis = 0
			for qy in range(y - 1, -1, -1):
				top_vis += 1
				if i[qy][x] >= curr:
					break
			bot_vis = 0
			for qy in range(y + 1, len(i)):
				bot_vis += 1
				if i[qy][x] >= curr:
					break
			left_vis = 0
			for qx in range(x - 1, -1, -1):
				left_vis += 1
				if i[y][qx] >= curr:
					break
			right_vis = 0
			for qx in range(x + 1, len(i[0])):
				right_vis += 1
				if i[y][qx] >= curr:
					break
			#print(f"y {y} x {x} tv {top_vis} bv {bot_vis} lv {left_vis} rv {right_vis}")
			sscore[y][x] = top_vis * bot_vis * left_vis * right_vis
	b = 0
	for y in range(len(i)):
		for x in range(len(i[0])):
			if sscore[y][x] > b:
				b = sscore[y][x]
	return b

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
