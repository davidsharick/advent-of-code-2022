#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 10)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 29)
		pass

def parse_input(i):
	for line in i:
		return line
	return 

def part1(i):
	last3 = []
	idx = 0
	for idx, c in enumerate(i):
		if len(last3) == 3 and c not in last3:
			if last3[0] != last3[1] and last3[0] != last3[2] and last3[1] != last3[2]:
				return idx + 1
		if len(last3) < 3:
			last3.append(c)
		else:
			del last3[0]
			last3.append(c)
	


def part2(i):
	last13 = []
	idx = 0
	for idx, c in enumerate(i):
		if len(last13) == 13 and c not in last13:
			if len(set(last13)) == 13:
				return idx + 1
		if len(last13) < 13:
			last13.append(c)
		else:
			del last13[0]
			last13.append(c)

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
