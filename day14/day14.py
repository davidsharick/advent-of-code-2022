#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 24)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 93)
		pass

def parse_input(i):
	rocklines = []
	for line in i:
		coords = (x.strip() for x in line.strip().split(" -> "))
		coords = list(c.split(",") for c in coords)
		coords = list((int(c[1]), int(c[0])) for c in coords)
		for idx in range(len(coords) - 1):
			rocklines.append((coords[idx], coords[idx + 1]))
	return rocklines

def part1(i):
	max_y = 0
	max_x = 0
	min_x = math.inf
	for r in i:
		max_y = max(max_y, r[0][0], r[1][0])
		max_x = max(max_x, r[0][1], r[1][1])
		min_x = min(min_x, r[0][1], r[1][1])
	base = min_x
	grid = []
	for idx in range(max_y + 1):
		grid.append(list(0 for idx in range(max_x - base + 1)))
	for r in i:
		grid = add_line(grid, r, base)
	c = 0
	while True:
		new_sand = add_sand(grid, max_y, base)
		c += 1
		if new_sand is None:
			break
		grid[new_sand[0]][new_sand[1]] = 2
	return c - 1

def add_line(grid, r, base):
	for y in range(min(r[0][0], r[1][0]), max(r[0][0], r[1][0]) + 1):
		for x in range(min(r[0][1], r[1][1]), max(r[0][1], r[1][1]) + 1):
			grid[y][x - base] = 1
	return grid

def add_sand(grid, max_y, base):
	sand_pos = [0, 500 - base]
	try:
		while True:
			if grid[sand_pos[0] + 1][sand_pos[1]] == 0:
				sand_pos[0] += 1
			elif grid[sand_pos[0] + 1][sand_pos[1] - 1] == 0:
				sand_pos[0] += 1
				sand_pos[1] -= 1
			elif grid[sand_pos[0] + 1][sand_pos[1] + 1] == 0:
				sand_pos[0] += 1
				sand_pos[1] += 1
			else:
				return sand_pos
	except:
		return None


def part2(i):
	max_y = 0
	max_x = 0
	min_x = math.inf
	for r in i:
		max_y = max(max_y, r[0][0], r[1][0])
		max_x = max(max_x, r[0][1], r[1][1])
		min_x = min(min_x, r[0][1], r[1][1])
	max_x += 1000
	min_x -= 1000
	max_y += 2
	base = min_x
	grid = []
	for idx in range(max_y + 1):
		grid.append(list(0 for idx in range(max_x - base + 1)))
	for r in i:
		grid = add_line(grid, r, base)
	grid = add_line(grid, [[max_y, min_x], [max_y, max_x]], base)
	c = 0
	while True:
		new_sand = add_sand(grid, max_y, base)
		c += 1
		if new_sand[0] == 0 and new_sand[1] == 500 - base:
			break
		grid[new_sand[0]][new_sand[1]] = 2
	return c

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
