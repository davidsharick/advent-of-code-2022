#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """root: pppw + sjmn
dbpl: 5
cczh: sllz + lgvd
zczc: 2
ptdq: humn - dvpt
dvpt: 3
lfqf: 4
humn: 5
ljgn: 2
sjmn: drzm * dbpl
sllz: 4
pppw: cczh / lfqf
lgvd: ljgn * ptdq
drzm: hmdt - zczc
hmdt: 32"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test, "root"), 152)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 301)
		pass

def parse_input(i):
	exprs = []
	for line in i:
		exprs.append(line.strip().replace(":", " ="))
	return exprs

def part1(i, goal):
	idx = 0
	vars = {}
	i2 = []
	for idx in range(len(i)):
		t = i[idx].split()
		try:
			q = int(t[-1])
			vars[t[0]] = q
		except:
			i2.append(i[idx])
	idx = 0
	while len(i2) > 0:
		try:
			t = i2[idx].split()
			v = t[0]
			a1 = vars[t[2]]
			a2 = vars[t[4]]
			vars[v] = eval(f"{a1} {t[3]} {a2}")
			i2.pop(idx)
			idx -= 1
		except:
			idx += 1
			if idx >= len(i2):
				idx = 0
	return int(vars[goal])

def part2(i):
	idx = 0
	vars = {}
	i2 = []
	needs = []
	dct = {}
	for idx in range(len(i)):
		t = i[idx].split()
		#if t[0] == "humn":
		#	continue
		if t[0] == "root":
			needs.append(t[2])
			needs.append(t[4])
			#continue
		try:
			q = int(t[-1])
			vars[t[0]] = q
		except:
			i2.append(i[idx])
	for expr in i2:
		e = expr.split()
		dct[e[0]] = [e[2], e[3], e[4]]
	g = 0
	if has(needs[0], dct, "humn"):
		g = 1
	else:
		g = 0
	idx = 0
	i3 = i2.copy()
	while len(i2) > 0:
		try:
			t = i2[idx].split()
			v = t[0]
			a1 = vars[t[2]]
			a2 = vars[t[4]]
			vars[v] = eval(f"{a1} {t[3]} {a2}")
			i2.pop(idx)
			idx -= 1
		except:
			idx += 1
			if idx >= len(i2):
				idx = 0
	need_num = vars[needs[g]]
	need_num = int(need_num)
	curr = needs[0 if g == 1 else 0]
	while True:
		needs = [dct[curr][0], dct[curr][2]]
		if has(needs[0], dct, "humn"):
			g = 1
		else:
			g = 0
		s = part1(i, needs[g])
		if dct[curr][1] == "+":
			need_num -= s
		elif dct[curr][1] == "-":
			if g == 0:
				need_num = s - need_num
			else:
				need_num += s
		elif dct[curr][1] == "*":
			need_num //= s
		else:
			if g == 0:
				need_num = s / need_num
			else:
				need_num *= s
		curr = needs[1 if g == 0 else 0]
		if curr == "humn":
			return int(need_num)

def has(root, dct, want):
	if root == want:
		return True
	elif root not in dct.keys():
		return False
	else:
		return has(dct[root][0], dct, want) or has(dct[root][2], dct, want)

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i, "root"))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
