#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """2,2,2
1,2,2
3,2,2
2,1,2
2,3,2
2,2,1
2,2,3
2,2,4
2,2,6
1,2,5
3,2,5
2,1,5
2,3,5"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 64)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 58)
		pass

def parse_input(i):
	q = []
	for line in i:
		q.append([int(x) for x in line.strip().split(",")])
	return q

def part1(i):
	box = []
	for _ in range(20):
		r = []
		for _ in range(20):
			r.append([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
		box.append(r)
	#box = list(list(list(0 for _ in range(20)) for _ in range(20)) for _ in range 20)
	#print(box)
	for c in i:
		box[c[0]][c[1]][c[2]] = 1
	v = 0
	for c in i:
		a = True
		if c[0] == 0 or box[c[0] - 1][c[1]][c[2]] == 0:
			v += 1
		if c[0] == 19 or box[c[0] + 1][c[1]][c[2]] == 0:
			v += 1
		if c[1] == 0 or box[c[0]][c[1] - 1][c[2]] == 0:
			v += 1
		if c[1] == 19 or box[c[0]][c[1] + 1][c[2]] == 0:
			v += 1
		if c[2] == 0 or box[c[0]][c[1]][c[2] - 1] == 0:
			v += 1
		if c[2] == 19 or box[c[0]][c[1]][c[2] + 1] == 0:
			v += 1
			"""
		if c[1] > 0:
			if box[c[0]][c[1] - 1][c[2]] != 0:
				continue
		if c[1] < 19:
			if box[c[0]][c[1] + 1][c[2]] != 0:
				continue
		if c[2] > 0:
			if box[c[0]][c[1]][c[2] - 1] != 0:
				continue
		if c[2] < 19:
			if box[c[0]][c[1]][c[2] + 1] != 0:
				continue"""
		#v += 1
	return v

def part2(i):
	box = []
	for _ in range(20):
		r = []
		for _ in range(20):
			r.append([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
		box.append(r)
	for c in i:
		box[c[0]][c[1]][c[2]] = 1
	v = 0
	explored = Counter()
	explored[(0,0,0)] = 1
	curr = (0,0,0)
	queue = []
	while True:
		explored[curr] = 1
		neighbors = get_neighbors(curr)
		neighbors = [n for n in neighbors if explored[n] < 1]
		for n in neighbors:
			if box[n[0]][n[1]][n[2]] == 0:
				queue.append(n)
		if len(queue) == 0:
			break
		curr = queue.pop()
	for c in i:
		a = True
		if True:
			if c[0] == 0:
				v += 1
			elif box[c[0] - 1][c[1]][c[2]] == 0:
				if external((c[0] - 1, c[1], c[2]), box, explored):
					v += 1
			if c[0] == 19:
				v += 1
			elif box[c[0] + 1][c[1]][c[2]] == 0:
				if external((c[0] + 1, c[1], c[2]), box, explored):
					v += 1
			if c[1] == 0:
				v += 1
			elif box[c[0]][c[1] - 1][c[2]] == 0:
				if external((c[0], c[1] - 1, c[2]), box, explored):
					v += 1
			if c[1] == 19:
				v += 1
			elif box[c[0]][c[1] + 1][c[2]] == 0:
				if external((c[0], c[1] + 1, c[2]), box, explored):
					v += 1
			if c[2] == 0:
				v += 1
			elif box[c[0]][c[1]][c[2] - 1] == 0:
				if external((c[0], c[1], c[2] - 1), box, explored):
					v += 1
			if c[2] == 19:
				v += 1
			elif box[c[0]][c[1]][c[2] + 1] == 0:
				if external((c[0], c[1], c[2] + 1), box, explored):
					v += 1
			"""
		if c[1] > 0:
			if box[c[0]][c[1] - 1][c[2]] != 0:
				continue
		if c[1] < 19:
			if box[c[0]][c[1] + 1][c[2]] != 0:
				continue
		if c[2] > 0:
			if box[c[0]][c[1]][c[2] - 1] != 0:
				continue
		if c[2] < 19:
			if box[c[0]][c[1]][c[2] + 1] != 0:
				continue"""
		#v += 1
	return v

def get_neighbors(pos):
	o = []
	x = pos[0]
	y = pos[1]
	z = pos[2]
	if x != 0:
		o.append((x - 1, y, z))
	if x != 19:
		o.append((x + 1, y, z))
	if y != 0:
		o.append((x, y - 1, z))
	if y != 19:
		o.append((x, y + 1, z))
	if z != 0:
		o.append((x, y, z - 1))
	if z != 19:
		o.append((x, y, z + 1))
	"""if y != 0:
			if z != 0:
				o.append((x - 1, y - 1, z - 1))
			if z != 19:
				o.append((x - 1, y - 1, z + 1))
		if y != 19:
			if z != 0:
				o.append((x - 1, y + 1, z - 1))
			if z != 19:
				o.append((x - 1, y + 1, z + 1))
	if x != 19:
		if y != 0:
			if z != 0:
				o.append((x + 1, y - 1, z - 1))
			if z != 19:
				o.append((x + 1, y - 1, z + 1))
		if y != 19:
			if z != 0:
				o.append((x + 1, y + 1, z - 1))
			if z != 19:
				o.append((x + 1, y + 1, z + 1))"""
	return o

def external(pos, box, acc):
	#print(acc[(pos[0], pos[1], pos[2])])
	return acc[(pos[0], pos[1], pos[2])] == 1
	if pos[0] <= 0 or pos[0] >= 19 or pos[1] <= 0 or pos[1] >= 19 or pos[2] <= 0 or pos[2] >= 19:
		return True
	#print(box[2][0][5])
	#print(box[2][1][5])
	"""try:
		if box[pos[0] - 1][pos[1]][pos[2]] != 0:
			if box[pos[0] + 1][pos[1]][pos[2]] != 0:
				if box[pos[0]][pos[1] - 1][pos[2]] != 0:
					if box[pos[0]][pos[1] + 1][pos[2]] != 0:
						if box[pos[0]][pos[1]][pos[2] - 1] != 0:
							if box[pos[0]][pos[1]][pos[2] + 1] != 0:
								return False
	except:
		return False
	return True"""
	try:
		if all(box[x][pos[1]][pos[2]] == 0 for x in range(0, pos[0])):
			#print(pos)
			#print("XUp")
			return True
		if all(box[x][pos[1]][pos[2]] == 0 for x in range(pos[0] + 1, 20)):
			#print(pos)
			#print("XD")
			return True
		if all(box[pos[0]][x][pos[2]] == 0 for x in range(0, pos[1])):
			#print(pos)
			#print("YUp")
			return True
		if all(box[pos[0]][x][pos[2]] == 0 for x in range(pos[1] + 1, 20)):
			#print(pos)
			#print("YD")
			return True
		if all(box[pos[0]][pos[1]][x] == 0 for x in range(0, pos[2])):
			#print(pos)
			#print("ZUp")
			return True
		if all(box[pos[0]][pos[1]][x] == 0 for x in range(pos[2] + 1, 20)):
			#print(pos)
			#print("ZD")
			return True
	except:
		return True
	#print(pos)
	return False

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
