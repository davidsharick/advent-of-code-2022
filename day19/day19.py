#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest

global_best = 0

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """Blueprint 1:  Each ore robot costs 4 ore.  Each clay robot costs 2 ore.  Each obsidian robot costs 3 ore and 14 clay.  Each geode robot costs 2 ore and 7 obsidian.
Blueprint 2:  Each ore robot costs 2 ore.  Each clay robot costs 3 ore.  Each obsidian robot costs 3 ore and 8 clay.  Each geode robot costs 3 ore and 12 obsidian."""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 33)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 56 * 62)
		pass

def parse_input(i):
	bps = []
	for line in i:
		bp = []
		for t in line.strip().split():
			try:
				x = int(t.strip(":"))
				bp.append(x)
			except:
				pass
		bps.append(bp)
	return bps

def part1(i):
	global global_best
	T_MAX = 24
	s = 0
	for bp in i:
		global_best = 0
		id = bp[0]
		ore_cost = bp[1]
		clay_cost = bp[2]
		obs_cost_ore = bp[3]
		obs_cost_clay = bp[4]
		geode_cost_ore = bp[5]
		geode_cost_obs = bp[6]
		v, st = best_construct(tuple(bp), T_MAX, 1, 0, 0, 0, 0, 0, 0, 0)
		s += (v * bp[0])
	return s

x = 0

def bc2(bp, mins, orebots, claybots, obsbots, geobots, ore, clay, obs):
	assert ore >= 0
	assert orebots >= 0
	assert clay >= 0
	assert claybots >= 0
	assert obs >= 0
	assert obsbots >= 0
	assert geobots >= 0
	if obsbots == 0:
		t_geobot_obs = math.inf
		max_nongeo_ore = ore
	else:
		t_geobot_obs = math.ceil((bp[6] - obs) / obsbots)
		max_nongeo_ore = 0
		for i in range(ore + 1):
			t_with = math.ceil((bp[5] - (ore - i)) / orebots)
			if t_with <= t_geobot_obs:
				max_nongeo_ore = i
			else:
				break
	
	if claybots == 0:
		t_obsbot_cly = math.inf
		max_nonobs_ore = max_nongeo_ore
	else:
		t_obsbot_cly = math.ceil((bp[4] - clay) / claybots)
		max_nonobs_ore = 0
		for i in range(max_nongeo_ore + 1):
			t_with = math.ceil((bp[3] - (max_nongeo_ore - i)) / orebots)
			if t_with <= t_obsbot_cly:
				max_nonobs_ore = i
			else:
				break
	build_geo = min(ore // bp[5], obs // bp[6])

@lru_cache(5000000)
def best_construct(bp, mins, orebots, claybots, obsbots, geobots, ore, clay, obs, geo):
	assert ore >= 0
	assert orebots >= 0
	assert clay >= 0
	assert claybots >= 0
	assert obs >= 0
	assert obsbots >= 0
	assert geobots >= 0
	global x
	global global_best
	best_can = best_possible(mins, geobots, geo)
	if best_can <= global_best:
		return 0, "Quit"
	max_ore = ore // bp[1]
	max_clay = ore // bp[2]
	max_obs = min(ore // bp[3], clay // bp[4])
	max_geo = min(ore // bp[5], obs // bp[6])
	best = 0
	if mins == 0:
		return 0, ""
	if mins == 1:
		return geobots, " irrelevant"
	ore_opts = [0]
	clay_opts = [0]
	obs_opts = [0]
	geo_opts = [0]
	geo_build = 0
	obs_build = 0
	new_ore = ore
	base = f"{33 - mins}: "
	tstr = base
	if obs >= bp[6] and ore >= bp[5]:
		geo_build = 1
		geo_opts.append(1)
	if clay >= bp[4] and ore >= bp[3] and obsbots < bp[6]:
		obs_build = 1
		obs_opts.append(1)
	if ore >= bp[2] and geo_build == 0 and claybots < bp[4]:
		clay_opts.append(1)
	if ore >= bp[1] and geo_build == 0 and mins > bp[1] and orebots < max(bp[1], bp[2], bp[3], bp[5]):
		ore_opts.append(1)
	an = False
	for i in clay_opts:
		for j in ore_opts:
			for k in obs_opts:
				for l in geo_opts:
					if i + j + k + l > 1:
						continue
					next_ore = ore - (bp[1] * j) - (bp[2] * i) - (bp[3] * k) - (bp[5] * l)
					attempt, nstr = best_construct(bp, mins - 1, orebots + j, claybots + i, obsbots + k, geobots + l, next_ore + orebots, clay + claybots - (bp[4] * k), obs + obsbots - (bp[6] * l), geo + geobots)
					attempt += geobots
					if attempt > best:
						an = True
						best = attempt
						if attempt > global_best:
							global_best = attempt
						if l > 0:
							tstr = base + (f" geo, " + nstr)
						elif k > 0:
							tstr = base + (f" obs, " + nstr)
						elif i > 0:
							tstr = base + (f" clay, " + nstr)
						elif j > 0:
							tstr = base + (f" ore, " + nstr)
						else:
							tstr = base + nstr
	assert tstr != ""
	return best, tstr

def best_possible(mins, geobots, geo):
	return geo + (geobots * mins) + ((mins * (mins + 1)) // 2)

def part2(i):
	global global_best
	global_best = 0
	best_construct.cache_clear()
	T_MAX = 32
	s = 1
	for bp in i[0:3]:
		global_best = 0
		id = bp[0]
		ore_cost = bp[1]
		clay_cost = bp[2]
		obs_cost_ore = bp[3]
		obs_cost_clay = bp[4]
		geode_cost_ore = bp[5]
		geode_cost_obs = bp[6]
		v, st = best_construct(tuple(bp), T_MAX, 1, 0, 0, 0, 0, 0, 0, 0)
		s *= v
	return s

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
