#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.graph as graph

valve_data = {}
big = 0
tstart = timeit.default_timer()

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """Valve AA has flow rate=0; tunnels lead to valves DD, II, BB
Valve BB has flow rate=13; tunnels lead to valves CC, AA
Valve CC has flow rate=2; tunnels lead to valves DD, BB
Valve DD has flow rate=20; tunnels lead to valves CC, AA, EE
Valve EE has flow rate=3; tunnels lead to valves FF, DD
Valve FF has flow rate=0; tunnels lead to valves EE, GG
Valve GG has flow rate=0; tunnels lead to valves FF, HH
Valve HH has flow rate=22; tunnel leads to valve GG
Valve II has flow rate=0; tunnels lead to valves AA, JJ
Valve JJ has flow rate=21; tunnel leads to valve II"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 1651)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 1707)
		pass

def parse_input(i):
	valves = []
	paths = {}
	for line in i:
		tokens = line.strip().split()
		id = tokens[1]
		fr = int(tokens[4][5:-1])
		out = [token.strip(",") for token in tokens[9:]]
		valves.append((id, fr))
		paths[id] = list(graph.Edge(e, 1) for e in out)
	#print(valves)
	#print(paths)
	return (valves, paths)

def part1(i):
	valves = i[0]
	paths = i[1]
	#for k, v in paths.items():
	#	v2 = list([graph.Edge(x.dest, 1) for x in v])
	#	paths[k] = v2
	#print(paths)
	start = "AA"
	g = graph.Graph(list([v[0] for v in valves]), paths)
	#print(g.adj)
	valve_data = {}
	for valve in valves:
		if valve[1] != 0 or valve[0] == "AA":
			#print(valve)
			vdists, vpaths = g.dijkstra(valve[0])
			#print(vdists)
			#print(vpaths)
			valve_data[valve[0]] = (valve[1], vdists, vpaths)
	#print(valve_data)
	n, p = recursive_search(30, [], "AA", valve_data, 0)
	#print(p)
	return n

#@lru_cache(None)
def recursive_search(mins_left, open_already, curr_valve, valve_data, already_pressure):
	best_path = ""
	if already_pressure > 2000:
		pass
	if len(open_already) == len(valve_data.keys()) - 1:
		#print(f"Searching with {mins_left} mins left, opened {open_already}, at {curr_valve}, pressure {already_pressure}, went to {already_pressure + pressure_time(open_already, valve_data, mins_left)}")
		return already_pressure + pressure_time(open_already, valve_data, mins_left), ""
	if mins_left == 0:
		return already_pressure, ""
	else:
		best = already_pressure
		for v in valve_data.keys():
			if v not in open_already and valve_data[v][0] != 0:
				#print("VD")
				#print(valve_data)
				dist = valve_data[curr_valve][1][v]
				if dist <= mins_left - 1:
					new_pr = already_pressure + pressure_time(open_already, valve_data, dist + 1)
					#if new_pr > 1000:
					#	print(new_pr)
					rs, st = recursive_search(mins_left - (dist + 1), open_already + [v], v, valve_data, new_pr)
					if rs > best:
						best = max(best, rs)
						best_path = f"{v} at {mins_left - (dist + 1)} " + st
		#if best > 5000 or (len(open_already) > 0 and open_already[0] == "DD"):# and open_already[1] == "BB" and open_already[2] == "JJ"):#or (len(open_already) > 2 and open_already[0] == "DD" and open_already[1] == "BB" and open_already[2] == "JJ"):
		#	print(f"Searching with {mins_left} mins left, opened {open_already}, at {curr_valve}, pressure {already_pressure}, went to {best}")
		return best, best_path

#@lru_cache(None)
def recursive_search_with_elephant(mins_left, open_already, curr_valve, elephant_dest, elephant_dist, already_pressure, options, simul = False):
	global valve_data
	global big
	path = ""
	big += 1
	if big % 1000000 == 0:
		print(timeit.default_timer() - tstart)
	#print(f"Searching with {mins_left} mins left, opened {open_already}, at {curr_valve}, pressure {already_pressure}, elephant arriving at {elephant_dest} in {elephant_dist}, went to {already_pressure + pressure_time(open_already, valve_data, mins_left)}")
	#if mins_left == 25:
	#	print(curr_valve)
	#if simul is None:
	#	print(f"Postsimuling with {mins_left} mins left, opened {open_already}, at {curr_valve}, pressure {already_pressure}, elephant arriving at {elephant_dest} in {elephant_dist}, went to {already_pressure + pressure_time(open_already, valve_data, mins_left)}")
	if mins_left == 0:
		if already_pressure > 2800:
			print(f"Resting with {mins_left} mins left, opened {open_already}, at {curr_valve}, pressure {already_pressure}, elephant arriving at {elephant_dest} in {elephant_dist}, went to {best + rt}")
		return already_pressure, ""
	else:
		best = already_pressure
		br = ""
		a = False
		assert curr_valve not in open_already
		assert len(open_already) == len(set(open_already))
		if simul:
			new_open = tuple(list(open_already) + [curr_valve, elephant_dest])
			options = [n for n in options if n != curr_valve and n != elephant_dest]
		else:
			new_open = tuple(list(open_already) + [curr_valve])
			options = [n for n in options if n != curr_valve]
		for v in options:
			if v not in open_already and valve_data[v][0] != 0 and v != elephant_dest:
				a = True
				dist = valve_data[curr_valve][1][v]
				if dist > mins_left or dist > 4:
					continue
				if not simul:
					if dist == elephant_dist:
						new = already_pressure + pressure_time(new_open, valve_data, min(dist + 1, elephant_dist + 1, mins_left))
						rs, st = recursive_search_with_elephant(mins_left - (dist + 1), new_open, elephant_dest, v, elephant_dist - (dist + 1), new, options, simul = True)
						if rs > best:
							best = rs
							path = f"{curr_valve} at {mins_left} " + st
					elif dist < elephant_dist:
						new = already_pressure + pressure_time(new_open, valve_data, min(dist + 1, elephant_dist + 1, mins_left))
						rs, st = recursive_search_with_elephant(mins_left - (dist + 1), new_open, v, elephant_dest, elephant_dist - (dist + 1), new, options)
						if rs > best:
							best = rs
							path = f"{curr_valve} at {mins_left} " + st
					elif dist > elephant_dist:
						new = already_pressure + pressure_time(new_open, valve_data, min(dist + 1, elephant_dist + 1, mins_left))
						rs, st = recursive_search_with_elephant(mins_left - (elephant_dist + 1), new_open, elephant_dest, v, dist - (elephant_dist + 1), new, options)
						if rs > best:
							best = rs
							path = f"{curr_valve} at {mins_left} " + st
					else:
						assert False
				else:
					#print(f"SImuling with {mins_left} mins left, opened {open_already}, at {curr_valve}, pressure {already_pressure}, elephant arriving at {elephant_dest} in {elephant_dist}, went to {already_pressure + pressure_time(open_already, valve_data, mins_left)}")
					for v2 in options:
						if v2 not in open_already and valve_data[v2][0] != 0 and v2 != v:
							#print(v2)
							#print(dist)
							dist2 = valve_data[elephant_dest][1][v2]
							if dist2 > mins_left or dist > 4:
								continue
							#print(dist2)
							if dist2 > dist:
								new = already_pressure + pressure_time(new_open, valve_data, min(dist + 1, mins_left))
								#print(new)
								#print(min(dist, dist2))
								rs, st = recursive_search_with_elephant(mins_left - (dist + 1), new_open, v, v2, dist2 - (dist + 1), new, options, simul = None)
								if rs > best:
									best = rs
									path = f"{curr_valve} and {elephant_dest} at {mins_left} " + st
							elif dist2 < dist:
								new = already_pressure + pressure_time(new_open, valve_data, min(dist2 + 1, mins_left))
								#print(new)
								#print(min(dist, dist2))
								rs, st = recursive_search_with_elephant(mins_left - (dist2 + 1), new_open, v2, v, dist - (dist2 + 1), new, options, simul = None)
								if rs > best:
									best = rs
									path = f"{curr_valve} and {elephant_dest} at {mins_left} " + st
							elif dist2 == dist:
								new = already_pressure + pressure_time(new_open, valve_data, min(dist2 + 1, mins_left))
								rs, st = recursive_search_with_elephant(mins_left - (dist + 1), new_open, v2, v, dist2 - (dist + 1), new, options, simul = True)
								if rs > best:
									best = rs
									path = f"{curr_valve} and {elephant_dest} at {mins_left} " + st
							else:
								assert False
		#print(f"{br} {best}")
		#print(f"Resting with {mins_left} mins left, opened {open_already}, at {curr_valve}, pressure {already_pressure}, elephant arriving at {elephant_dest} in {elephant_dist}, {options}")
		if not a:
			#print("Rest")
			rt = rest_time(open_already, valve_data, mins_left, curr_valve, elephant_dest, elephant_dist)
			#print(rt)
			#print(mins_left)
			#if best + rt > 20:
				#print(open_already)
				#print(curr_valve)
				#print(elephant_dest)
				#print(f"Resting with {mins_left} mins left, opened {open_already}, at {curr_valve}, pressure {already_pressure}, elephant arriving at {elephant_dest} in {elephant_dist}, went to {best + rt}")
				#print(rt)
			path += f"{curr_valve} at {mins_left} and {elephant_dest} at {mins_left - (elephant_dist + 1)}"
			if best + rt > 2800:
				print(f"Resting with {mins_left} mins left, opened {open_already}, at {curr_valve}, pressure {already_pressure}, elephant arriving at {elephant_dest} in {elephant_dist}, went to {best + rt}")
			return best + rt, path
		#print(best)
		if best > 2800:
			print(f"Resting with {mins_left} mins left, opened {open_already}, at {curr_valve}, pressure {already_pressure}, elephant arriving at {elephant_dest} in {elephant_dist}, went to {best}, p {path}")
		return best, path
	"""if len(open_already) == len(valve_data.keys()) - 1:
		#print(f"Searching with {mins_left} mins left, opened {open_already}, at {curr_valve}, pressure {already_pressure}, went to {already_pressure + pressure_time(open_already, valve_data, mins_left)}")
		return already_pressure + pressure_time(open_already, valve_data, mins_left)
	if mins_left == 0:
		return already_pressure
	else:
		#print(curr_valve)
		if (valve_data[curr_valve][0] != 0 and curr_valve not in open_already) and (valve_data[elephant_valve][0] != 0 and curr_valve not in open_already):
			# open both
			return recursive_search_with_elephant(mins_left - 1, tuple(list(open_already) + [curr_valve, elephant_valve]), curr_valve, elephant_valve, already_pressure + pressure_time(open_already, valve_data, 1))
		elif valve_data[elephant_valve][0] != 0 and elephant_valve not in open_already:
			# open it
			best = already_pressure
			for vlv in valve_data[curr_valve][3]:
				new = recursive_search_with_elephant(mins_left - 1, tuple(list(open_already) + [elephant_valve]), vlv[0], elephant_valve, already_pressure + pressure_time(open_already, valve_data, 1))
				best = max(new, best)
			return best
		elif valve_data[curr_valve][0] != 0 and curr_valve not in open_already:
			# open it
			best = already_pressure
			for vlv in valve_data[elephant_valve][3]:
				new = recursive_search_with_elephant(mins_left - 1, tuple(list(open_already) + [curr_valve]), elephant_valve, vlv[0], already_pressure + pressure_time(open_already, valve_data, 1))
				best = max(new, best)
			return best
		else:
			best = already_pressure
			for vlv in valve_data[curr_valve][3]:
				# can move to any of them
				for vlv2 in valve_data[elephant_valve][3]:
					if vlv != vlv2:
						new = recursive_search_with_elephant(mins_left - 1, tuple(list(open_already)), vlv[0], vlv2[0], already_pressure + pressure_time(open_already, valve_data, 1))
						best = max(new, best)
			return best"""

def pressure_time(valves, data, time):
	t = 0
	for v in valves:
		t += data[v][0]
	return t * time

def rest_time(valves, data, time, current, elephant, elephant_dist):
	base = pressure_time(valves, data, time)
	#print(valves)
	#print(base)
	base += ((time) * (data[current][0]))
	#print(base)
	if time >= elephant_dist:
		base += ((time - (elephant_dist + 1)) * data[elephant][0])
	#print(base)
	return base

def part2(i):
	global valve_data
	valves = i[0]
	paths = i[1]
	for k, v in paths.items():
		v2 = list([graph.Edge(x.dest, 1) for x in v])
		paths[k] = v2
	#print(paths)
	start = "AA"
	g = graph.Graph(list([v[0] for v in valves]), paths)
	#print(g.adj)
	valve_data = {}
	for valve in valves:
		if valve[1] != 0 or valve[0] == "AA":
			vdists, vpaths = g.dijkstra(valve[0])
			valve_data[valve[0]] = [valve[1], vdists, vpaths, paths[valve[0]]]
	#for v in valve_data.keys():
	#	print(v)
	#	print(valve_data[v])
	"""for v in valve_data.keys():
		print(v)
		print(valve_data[v][0])
		print(paths[v])
		print(valve_data[v][1])
		for p in valve_data.keys():
			if valve_data[p][0] != 0 or p == "AA":
				if valve_data[v][1][p] == 4:
					print(f"To {p} in {valve_data[v][1][p]}")
		#print(valve_data[v])"""
	best = 0
	for v in valve_data.keys():
		if v != "AA":
			#print(v)
			curr, path = recursive_search_with_elephant(26, (), "AA", v, valve_data["AA"][1][v], 0, list(valve_data.keys()))
			#print(curr)
			#print(path)
			best = max(best, curr)
			#print(best)
	return best

# above 2650
# not 2649/2651/2652/2653/2654/2655/2656/2657/2658/2659/2660/2661/2662/2663/2664/2665/2666/2667/2668/2669/2670/2671/2672/2673/2674/2675/2676/2677/2678
# below 2700

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	#print(part2(i))
	print(2679)
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
