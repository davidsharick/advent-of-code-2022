#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2"""
		self.test = parse_input(self.testinput.split("\n"))
		self.testinput2 = """R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20"""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 13)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 1)
		self.assertEqual(part2(self.test2), 36)
		pass

def parse_input(i):
	steps = []
	for line in i:
		steps.append(line.strip().split())
	return steps

def part1(i):
	tail_pos = (0, 0)
	head_pos = (0, 0)
	prev_tail = [(0, 0)]
	for step in i:
		dir = step[0]
		num = int(step[1])
		for i in range(num):
			if dir == "D":
				head_pos = (head_pos[0], head_pos[1] - 1)
				if head_pos[1] <= tail_pos[1] - 2:
					tail_pos = (tail_pos[0], tail_pos[1] - 1)
					if head_pos[0] != tail_pos[0]:
						tail_pos = (head_pos[0], tail_pos[1])
			if dir == "U":
				head_pos = (head_pos[0], head_pos[1] + 1)
				if head_pos[1] >= tail_pos[1] + 2:
					tail_pos = (tail_pos[0], tail_pos[1] + 1)
					if head_pos[0] != tail_pos[0]:
						tail_pos = (head_pos[0], tail_pos[1])
			if dir == "L":
				head_pos = (head_pos[0] - 1, head_pos[1])
				if head_pos[0] <= tail_pos[0] - 2:
					tail_pos = (tail_pos[0] - 1, tail_pos[1])
					if head_pos[1] != tail_pos[1]:
						tail_pos = (tail_pos[0], head_pos[1])
			if dir == "R":
				head_pos = (head_pos[0] + 1, head_pos[1])
				if head_pos[0] >= tail_pos[0] + 2:
					tail_pos = (tail_pos[0] + 1, tail_pos[1])
					if head_pos[1] != tail_pos[1]:
						tail_pos = (tail_pos[0], head_pos[1])
			prev_tail.append(tail_pos)
	return len(set(prev_tail))

def part2(i):
	positions = [(0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0), (0, 0)]
	last_step = []
	tail_pos = []
	for step in i:
		head_pos = positions[0]
		dir = step[0]
		num = int(step[1])
		for i in range(num):
			if dir == "D":
				head_pos = (head_pos[0], head_pos[1] - 1)
				positions[0] = head_pos
				positions = advance_further(positions, dir)
			elif dir == "U":
				head_pos = (head_pos[0], head_pos[1] + 1)
				positions[0] = head_pos
				positions = advance_further(positions, dir)
			elif dir == "L":
				head_pos = (head_pos[0] - 1, head_pos[1])
				positions[0] = head_pos
				positions = advance_further(positions, dir)
			elif dir == "R":
				head_pos = (head_pos[0] + 1, head_pos[1])
				positions[0] = head_pos
				positions = advance_further(positions, dir)
			tail_pos.append(positions[-1])
	return len(set(tail_pos))


def advance_further(positions, dir):
	for i in range(1, 10):
		dist = apartness(positions[i], positions[i - 1]) # 2: diag big, 1: linear big, 0: not
		if dist == 2:
			positions[i] = (positions[i][0] + 1 if positions[i - 1][0] > positions[i][0] else positions[i][0] - 1, positions[i][1] + 1 if positions[i - 1][1] > positions[i][1] else positions[i][1] - 1)
		elif dist == 1:
			if positions[i][0] == positions[i - 1][0]:
				positions[i] = (positions[i][0], positions[i][1] + 1 if positions[i - 1][1] > positions[i][1] else positions[i][1] - 1)
			else:
				positions[i] = (positions[i][0] + 1 if positions[i - 1][0] > positions[i][0] else positions[i][0] - 1, positions[i][1])
		#print(f"pos {positions[i]} i {i}")
	return positions

def apartness(pos1, pos2):
	if abs(pos1[0] - pos2[0]) > 1:
		return 2 if pos1[1] != pos2[1] else 1
	elif abs(pos1[1] - pos2[1]) > 1:
		return 2 if pos1[0] != pos2[0] else 1
	return 0

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
