#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 13140)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), """##..##..##..##..##..##..##..##..##..##..
###...###...###...###...###...###...###.
####....####....####....####....####....
#####.....#####.....#####.....#####.....
######......######......######......####
#######.......#######.......#######.....""")
		pass

def parse_input(i):
	cmds = []
	for line in i:
		if line.strip() == "noop":
			cmds.append("noop")
		else:
			cmds.append(list(line.strip().split()))
	return cmds

def part1(i):
	cycle = 0
	reg = 1
	sum = 0
	for cmd in i:
		if cmd == "noop":
			cycle += 1
			if interesting(cycle) and cycle <= 220:
				sum += (cycle * reg)
		else:
			cycle += 1
			if interesting(cycle) and cycle <= 220:
				sum += (cycle * reg)
			cycle += 1
			if interesting(cycle) and cycle <= 220:
				sum += (cycle * reg)
			reg += int(cmd[1])
	return sum

def interesting(cycle):
	return (cycle == 20) or ((cycle - 20) % 40 == 0)

def part2(i):
	img = [["." for _ in range(40)] for _ in range(6)]
	cycle = 0
	reg = 1
	sum = 0
	for cmd in i:
		if cmd == "noop":
			sprite_pos = reg
			cur_draw = cycle % 40
			if sprite_pos == cur_draw or sprite_pos == cur_draw - 1 or sprite_pos == cur_draw + 1:
				img[cycle // 40][cur_draw % 40] = "#"
			cycle += 1
		else:
			sprite_pos = reg
			cur_draw = cycle % 40
			if sprite_pos == cur_draw or sprite_pos == cur_draw - 1 or sprite_pos == cur_draw + 1:
				img[cycle // 40][cur_draw % 40] = "#"
			cycle += 1
			sprite_pos = reg
			cur_draw = cycle % 40
			if sprite_pos == cur_draw or sprite_pos == cur_draw - 1 or sprite_pos == cur_draw + 1:
				img[cycle // 40][cur_draw % 40] = "#"
			cycle += 1
			reg += int(cmd[1])
	out = ""
	for r in img:
		s = ""
		for c in r:
			s += c
		out += s
		out += "\n"
	return out.strip()

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
