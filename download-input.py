#!/usr/bin/env python3

import sys
import requests

def main():
    day = int(sys.argv[1])
    with open("../config") as f:
        session = f.readline().strip()
    cookies = {"session": session}
    headers = {"User-Agent": "David Sharick Script https://gitlab.com/davidsharick/advent-of-code-2022 david.le.sharick@gmail.com"}
    r = requests.get(f"https://adventofcode.com/2022/day/{day}/input", cookies = cookies, headers = headers)
    with open(f"day{day}/day{day}-input.txt", "w") as f:
        f.write(r.text)


if __name__ == '__main__':
    main()