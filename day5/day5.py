#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), "CMZ")
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), "MCD")
		pass

def parse_input(i):
	idx = 0
	stack_num = 9 #3
	stacks = []
	for _ in range(stack_num):
		stacks.append([])
	rules = []
	for line in i:
		if idx < 8: #3
			for ind, chr in enumerate(line):
				if ind % 4 == 1 and chr != ' ':
					stacks[ind // 4].append(chr)
		elif idx >= 10: #5
			r = []
			for token in line.strip().split():
				try:
					r.append(int(token))
				except:
					pass
			rules.append(r)
		idx += 1
	for stack in stacks:
		stack.reverse()
	return (stacks, rules)

def part1(i):
	stacks = i[0]
	rules = i[1]
	for rule in rules:
		for _ in range(rule[0]):
			try:
				v = stacks[rule[1] - 1].pop()
				stacks[rule[2] - 1].append(v)
			except:
				pass
	out = ""
	for stack in stacks:
		out += stack[-1]
	return out

def part2(i):
	stacks = i[0]
	rules = i[1]
	for rule in rules:
		temp = []
		for _ in range(rule[0]):
			try:
				v = stacks[rule[1] - 1].pop()
				temp.append(v)
			except:
				pass
		temp.reverse()
		for v in temp:
			stacks[rule[2] - 1].append(v)
	out = ""
	for stack in stacks:
		try:
			out += stack[-1]
		except:
			pass
	return out

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
