#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid

global_best = math.inf

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """#.######
#>>.<^<#
#.<..<<#
#>v.><>#
#<^v^^>#
######.#"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 18)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 54)
		pass

def parse_input(i):
	g = []
	for line in i:
		r = []
		for c in line.strip():
			if c == "#":
				r.append(1)
			elif c == ".":
				r.append(0)
			elif c == "^":
				r.append(["up"])
			elif c == "v":
				r.append(["down"])
			elif c == ">":
				r.append(["right"])
			elif c == "<":
				r.append(["left"])
			else:
				assert False
		g.append(r)
	return g

def part1(i):
	g = i
	pos = (0, 1)
	rn = 0
	best = find_fastest(pos, g, (len(g) - 2) * (len(g[0]) - 2))
	return best

def find_fastest(start_pos, start_g, max_depth):
	blizzards_at_t = {0: start_g}
	new_max = max_depth
	s = 0
	for t in range(1, max_depth):
		nxt = move_blizz(blizzards_at_t[t - 1])
		blizzards_at_t[t] = nxt
		if t % (len(start_g) - 2) == 0 or t % (len(start_g[0]) - 2) == 0:
			for t2 in range(0, t):
				if blizzards_at_t[t2] == blizzards_at_t[t]:
					new_max = t
					s = 1
					break
			if s != 0:
				break
	assert blizzards_at_t[0] == move_blizz(blizzards_at_t[new_max - 1])
	queue = [(start_pos[0] + 1, start_pos[1], 1)]
	y_max = len(start_g) - 1
	x_max = len(start_g[0]) - 1
	full_max = new_max * x_max * y_max
	seen = []
	best_times = Counter()
	best_times[(start_pos[0], start_pos[1])] = 0
	nxt = [(queue[0][0], queue[0][1])]
	for t in range(full_max):
		nnxt = set()
		for n in nxt:
			ns = grid.grid_neighbors(n, (1, y_max), (1, x_max - 1), corners = False)
			for n2 in ns:
				if n2[0] == y_max and n2[1] == x_max - 1:
					return t + 1
				nnxt.add(n2)
			nnxt.add(n)
		nnxt = [n for n in nnxt if blizzards_at_t[(t + 1) % new_max][n[0]][n[1]] == 0]
		nxt = nnxt
	return -10000


def find_fastest_redouble(start_pos, start_g, max_depth):
	blizzards_at_t = {0: start_g}
	new_max = max_depth
	s = 0
	goals_met = 0
	for t in range(1, max_depth):
		nxt = move_blizz(blizzards_at_t[t - 1])
		blizzards_at_t[t] = nxt
		if t % (len(start_g) - 2) == 0 or t % (len(start_g[0]) - 2) == 0:
			for t2 in range(0, t):
				if blizzards_at_t[t2] == blizzards_at_t[t]:
					new_max = t
					s = 1
					break
			if s != 0:
				break
	assert blizzards_at_t[0] == move_blizz(blizzards_at_t[new_max - 1])
	queue = [(start_pos[0] + 1, start_pos[1], 1)]
	y_max = len(start_g) - 1
	x_max = len(start_g[0]) - 1
	full_max = new_max * x_max * y_max
	seen = []
	best_times = Counter()
	best_times[(start_pos[0], start_pos[1])] = 0
	nxt = [(queue[0][0], queue[0][1])]
	c = False
	for t in range(full_max):
		nnxt = set()
		for n in nxt:
			ns = grid.grid_neighbors(n, (0, y_max), (1, x_max - 1), corners = False)
			for n2 in ns:
				if n2[0] == y_max and n2[1] == x_max - 1 and not c:
					if goals_met == 0:
						goals_met += 1
						c = True
						rnxt = n2
					elif goals_met == 2:
						return t + 1
				if n2[0] == start_pos[0] and n2[1] == start_pos[1] and goals_met == 1 and not c:
					goals_met += 1
					c = True
					rnxt = n2
				nnxt.add(n2)
			nnxt.add(n)
		nnxt = [n for n in nnxt if blizzards_at_t[(t + 1) % new_max][n[0]][n[1]] == 0]
		if c:
			nnxt = [rnxt]
			c = False
		nxt = nnxt
	return -10000

def move_blizz(g):
	ng = []
	for y in range(len(g)):
		nr = []
		for x in range(len(g[0])):
			if g[y][x] == 1:
				nr.append(1)
			else:
				nr.append([])
		ng.append(nr)
	for y in range(1, len(g) - 1):
		for x in range(1, len(g[0]) - 1):
			if type(g[y][x]) is list:
				for b in g[y][x]:
					if b == "up":
						if g[y - 1][x] == 1:
							new_pos = (len(g) - 2, x)
						else:
							new_pos = (y - 1, x)
						ng[new_pos[0]][new_pos[1]].append(b)
					elif b == "down":
						if g[y + 1][x] == 1:
							new_pos = (1, x)
						else:
							new_pos = (y + 1, x)
						ng[new_pos[0]][new_pos[1]].append(b)
					elif b == "left":
						if g[y][x - 1] == 1:
							new_pos = (y, len(g[0]) - 2)
						else:
							new_pos = (y, x - 1)
						ng[new_pos[0]][new_pos[1]].append(b)
					elif b == "right":
						if g[y][x + 1] == 1:
							new_pos = (y, 1)
						else:
							new_pos = (y, x + 1)
						ng[new_pos[0]][new_pos[1]].append(b)
					else:
						assert False
	for y in range(len(g)):
		for x in range(len(g[0])):
			if type(ng[y][x]) is list:
				if len(ng[y][x]) == 0:
					ng[y][x] = 0
	return ng
						

				

def part2(i):
	g = i
	pos = (0, 1)
	rn = 0
	best = find_fastest_redouble(pos, g, (len(g) - 2) * (len(g[0]) - 2))
	return best

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
