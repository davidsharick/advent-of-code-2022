#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 31)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 29)
		pass

def parse_input(i):
	grid = []
	for line in i:
		grid.append(list(c for c in line.strip()))
	return grid

def part1(i):
	grid2 = []
	verts = []
	size_y = (0, len(i) - 1)
	size_x = (0, len(i[0]) - 1)
	for row in range(len(i)):
		row2 = []
		for col in range(len(i[row])):
			if i[row][col] == "S":
				start_pos = (row, col)
				row2.append(1)
			elif i[row][col] == "E":
				end_pos = (row, col)
				row2.append(26)
			else:
				row2.append(ord(i[row][col]) - 96)
			verts.append((row, col))
		grid2.append(row2)
	dist = {start_pos: 0}
	prev = {}
	queue = [start_pos]
	for v in verts:
		if v != start_pos:
			dist[v] = math.inf
			prev[v] = None
			queue.append(v)
	
	while len(queue) > 0:
		queue = sorted(queue, key = lambda v: dist[v])
		smallest = queue[0]
		queue = queue[1:]
		for n in grid.grid_neighbors(smallest, size_y, size_x, corners = False):
			if (grid2[n[0]])[n[1]] - (grid2[smallest[0]])[smallest[1]] <= 1:
				new_dist = dist[smallest] + 1
				if new_dist < dist[n]:
					dist[n] = new_dist
					prev[n] = smallest
	return dist[end_pos]

def part2(i):
	grid2 = []
	verts = []
	size_y = (0, len(i) - 1)
	size_x = (0, len(i[0]) - 1)
	for row in range(len(i)):
		row2 = []
		for col in range(len(i[row])):
			if i[row][col] == "S":
				start_pos = (row, col)
				row2.append(1)
			elif i[row][col] == "E":
				end_pos = (row, col)
				row2.append(26)
			else:
				row2.append(ord(i[row][col]) - 96)
			verts.append((row, col))
		grid2.append(row2)
	dist = {end_pos: 0}
	prev = {}
	queue = [end_pos]
	for v in verts:
		if v != end_pos:
			dist[v] = math.inf
			prev[v] = None
			queue.append(v)
	
	while len(queue) > 0:
		queue = sorted(queue, key = lambda v: dist[v])
		smallest = queue[0]
		queue = queue[1:]
		for n in grid.grid_neighbors(smallest, size_y, size_x, corners = False):
			if (grid2[smallest[0]])[smallest[1]] - (grid2[n[0]])[n[1]] <= 1:
				new_dist = dist[smallest] + 1
				if new_dist < dist[n]:
					dist[n] = new_dist
					prev[n] = smallest
	best = math.inf
	for row in range(len(i)):
		for col in range(len(i[row])):
			pos = (row, col)
			if grid2[row][col] == 1:
				if dist[pos] < best:
					best = dist[pos]
	return best

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
