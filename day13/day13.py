#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache, cmp_to_key
import itertools
import math
import string
import sys
import timeit
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 13)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 140)
		pass

def parse_input(i):
	pairs = []
	for idx, line in enumerate(i):
		if idx % 3 == 0:
			p0 = eval(line.strip())
		elif idx % 3 == 1:
			p1 = eval(line.strip())
			pairs.append([p0, p1])
	return pairs

def part1(i):
	s = 0
	for idx, p in enumerate(i):
		if right_order(p[0], p[1]):
			s += (idx + 1)
	return s

def right_order(v1, v2):
	# v1 should be before v2
	if type(v1) == int and type(v2) == int:
		if v1 < v2:
			return True
		elif v1 == v2:
			return None
		else:
			return False
	elif type(v1) == list and type(v2) == list:
		for i in range(len(v1)):
			try:
				_ = v2[i]
			except:
				return False
			o = right_order(v1[i], v2[i])
			if o is not None:
				return o
		if len(v2) > len(v1):
			return True
		else:
			return None
	elif type(v1) == list and type(v2) == int:
		return right_order(v1, [v2])
	elif type(v1) == int and type(v2) == list:
		return right_order([v1], v2)
	return False

def ro2(v1, v2):
	x = right_order(v1, v2)
	return -1 if x else 1

def part2(i):
	packets = [[[2]], [[6]]]
	for p in i:
		packets.append(p[0])
		packets.append(p[1])
	x = sorted(packets, key = cmp_to_key(ro2))
	o = 1
	for idx, p in enumerate(x):
		if p == [[2]]:
			o *= (idx + 1)
		if p == [[6]]:
			o *= (idx + 1)
	return o

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
