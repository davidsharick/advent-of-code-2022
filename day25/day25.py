#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """1=-0-2
12111
2=0=
21
2=01
111
20012
112
1=-1=
1-12
12
1=
122"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), "2=-1=0")
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 0)
		pass

def parse_input(i):
	sl = []
	for line in i:
		sl.append(line.strip())
	return sl

def part1(i):
	rns = []
	for n in i:
		rns.append(snafu_to_real(n))
	q = sum(rns)
	return real_to_snafu(q)

def snafu_to_real(n):
	l = len(n)
	curr_place = 1
	z = 0
	for i in range(l - 1, -1, -1):
		if n[i] == "2":
			d = 2
		elif n[i] == "1":
			d = 1
		elif n[i] == "0":
			d = 0
		elif n[i] == "-":
			d = -1
		elif n[i] == "=":
			d = -2
		else:
			assert False
		z += (d * curr_place)
		curr_place *= 5
	return z

def real_to_snafu(n):
	curr_n = n
	o = ""
	while True:
		base_d = curr_n % 5
		if base_d == 0:
			o = "0" + o
			curr_n //= 5
		elif base_d == 1:
			o = "1" + o
			curr_n -= 1
			curr_n //= 5
		elif base_d == 2:
			o = "2" + o
			curr_n -= 2
			curr_n //= 5
		elif base_d == 3:
			o = "=" + o
			curr_n += 2
			curr_n //= 5
		elif base_d == 4:
			o = "-" + o
			curr_n += 1
			curr_n //= 5
		else:
			assert False
		if curr_n == 0:
			return o

def part2(i):
	return 0

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
