#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """1000
2000
3000

4000

5000
6000

7000
8000
9000

10000
"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 24000)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 45000)
		pass

def parse_input(i):
	x = []
	y = []
	for line in i:
		if len(line) < 2:
			x.append(y)
			y = []
		else:
			y.append(int(line.strip()))
	return x

def part1(i):
	m = 0
	for l in i:
		m = max(sum(l), m)
	return m 

def part2(i):
	l2 = []
	for l in i:
		l2.append(sum(l))
	l2.sort()
	return l2[-1] + l2[-2] + l2[-3]

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
