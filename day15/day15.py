#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """Sensor at x=2, y=18: closest beacon is at x=-2, y=15
Sensor at x=9, y=16: closest beacon is at x=10, y=16
Sensor at x=13, y=2: closest beacon is at x=15, y=3
Sensor at x=12, y=14: closest beacon is at x=10, y=16
Sensor at x=10, y=20: closest beacon is at x=10, y=16
Sensor at x=14, y=17: closest beacon is at x=10, y=16
Sensor at x=8, y=7: closest beacon is at x=2, y=10
Sensor at x=2, y=0: closest beacon is at x=2, y=10
Sensor at x=0, y=11: closest beacon is at x=2, y=10
Sensor at x=20, y=14: closest beacon is at x=25, y=17
Sensor at x=17, y=20: closest beacon is at x=21, y=22
Sensor at x=16, y=7: closest beacon is at x=15, y=3
Sensor at x=14, y=3: closest beacon is at x=15, y=3
Sensor at x=20, y=1: closest beacon is at x=15, y=3"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test, 10), 26)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test, 20), 56000011)
		pass

def parse_input(i):
	data = []
	for line in i:
		cd = []
		tokens = line.strip().split()
		for t in tokens:
			if t[1] == "=":
				cd.append(int(t[2:].strip(",").strip(":")))
		data.append(cd)
	return data

def part1(i, row):
	taxicabs = {}
	trolls = []
	maxtax = 0
	for dp in i:
		coord = (dp[1], dp[0])
		beac = (dp[3], dp[2])
		if dp[3] == row:
			trolls.append(dp[2])
		taxicab = tcd(coord, beac)
		taxicabs[coord] = taxicab
		maxtax = max(maxtax, taxicab)
	v = 0
	vals = []
	for base, dist in taxicabs.items():
		ydiff = abs(base[0] - row)
		if ydiff < dist:
			interval = [base[1] + (dist - ydiff), base[1] - (dist - ydiff)]
			vals.append(interval)
	for v in vals:
		v.sort()
	vals.sort(key = lambda k: k[0])
	new_vals = []
	m1 = vals[0][0]
	vals.sort(key = lambda k: k[1])
	m2 = vals[-1][1]
	return m2 - m1 + 1 - len(set(trolls))

def tcd(p1, p2):
	return abs(p1[1] - p2[1]) + abs(p1[0] - p2[0])

def get_vals(taxicabs, y):
	vals = []
	for base, dist in taxicabs.items():
		ydiff = abs(base[0] - y)
		if ydiff < dist:
			interval = [base[1] + (dist - ydiff), base[1] - (dist - ydiff)]
			vals.append(interval)
	return vals

def val_missing(vals, end):
	vals.sort(key = lambda k: k[0])
	curr_highest = 0
	for v in vals:
		if v[0] > curr_highest and curr_highest <= end:
			assert v[0] == curr_highest + 2
			return curr_highest + 1
		if v[1] > curr_highest:
			curr_highest = v[1]
	return None

def part2(i, sspace):
	taxicabs = {}
	trolls = []
	maxtax = 0
	for dp in i:
		coord = (dp[1], dp[0])
		beac = (dp[3], dp[2])
		taxicab = tcd(coord, beac)
		taxicabs[coord] = taxicab
		maxtax = max(maxtax, taxicab)
	for y in range(0, sspace + 1):
		vals = get_vals(taxicabs, y)
		for v in vals:
			v.sort()
		m = val_missing(vals, sspace)
		if m is not None:
			return (m * 4000000) + y
		#if y % 100000 == 0:
		#	print(y)

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i, 2000000))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i, 4000000))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
