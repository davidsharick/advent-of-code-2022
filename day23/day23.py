#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest
import utils.grid as grid

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """....#..
..###.#
#...#.#
.#...##
#.###..
##.#.##
.#..#.."""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		#self.assertEqual(part1(self.test), 110)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 20)
		pass

def parse_input(i):
	g = []
	for line in i:
		r = []
		for c in line.strip():
			r.append(1 if c == "#" else 0)
		g.append(r)
	return g

def part1(i):
	g = i
	cp = 0
	e = Counter()
	for y in range(len(g)):
		for x in range(len(g[0])):
			if g[y][x] != 0:
				e[(y, x)] = 1
	for i in range(10):
		e, cp = process_elves2(e, cp)
	min_y = math.inf
	max_y = 0
	min_x = math.inf
	max_x = 0
	for e2 in e:
		min_y = min(min_y, e2[0])
		max_y = max(max_y, e2[0])
		min_x = min(min_x, e2[1])
		max_x = max(max_x, e2[1])
	o = 0
	for y in range(min_y, max_y + 1):
		for x in range(min_x, max_x + 1):
			if e[(y, x)] == 0:
				o += 1
	return o

def pretty_print(g):
	s = ""
	for l in g:
		s = ""
		for c in l:
			if c == 1:
				s += "#"
			else:
				s += "."
		print(s)
	
def process_elves2(elves, cycle_pos):
	dests = []
	dest_dict = {}
	for e in elves.elements():
		y = e[0]
		x = e[1]
		ns = grid.grid_neighbors(e, None, None, borderless = True)
		dir = ""
		above = [n for n in ns if n[0] < y]
		below = [n for n in ns if n[0] > y]
		left = [n for n in ns if n[1] < x]
		right = [n for n in ns if n[1] > x]
		if all(elves[(p[0], p[1])] == 0 for p in ns):
			pass
		else:
			if all(elves[(p[0], p[1])] == 0 for p in above) and dir == "" and cycle_pos < 1:
				dir = "up"
			if all(elves[(p[0], p[1])] == 0 for p in below) and dir == "" and cycle_pos < 2:
				dir = "down"
			if all(elves[(p[0], p[1])] == 0 for p in left) and dir == "" and cycle_pos < 3:
				dir = "left"
			if all(elves[(p[0], p[1])] == 0 for p in right) and dir == "":
				dir = "right"
			if all(elves[(p[0], p[1])] == 0 for p in above) and dir == "" and cycle_pos > 0:
				dir = "up"
			if all(elves[(p[0], p[1])] == 0 for p in below) and dir == "" and cycle_pos > 1:
				dir = "down"
			if all(elves[(p[0], p[1])] == 0 for p in left) and dir == "" and cycle_pos > 2:
				dir = "left"
		if dir == "up":
			d = (y - 1, x)
		elif dir == "down":
			d = (y + 1, x)
		elif dir == "left":
			d = (y, x - 1)
		elif dir == "right":
			d = (y, x + 1)
		else:
			d = e
		dests.append(d)
		dest_dict[e] = d
	dc = Counter(dests)
	op = []
	moved = 0
	o = Counter()
	for e in elves:
		y = e[0]
		x = e[1]
		dest = dest_dict[e]
		if dc[dest] == 1:
			op.append(dest)
			if e != dest:
				moved += 1
			o[dest] = 1
		else:
			o[e] = 1
	
	if cycle_pos == 3:
		cycle_pos = 0
	else:
		cycle_pos += 1
	#print(moved)
	if moved == 0:
		return None, None
	return o, cycle_pos

def part2(i):
	g = i
	e = Counter()
	cp = 0
	rn = 0
	for y in range(len(g)):
		for x in range(len(g[0])):
			if g[y][x] != 0:
				e[(y, x)] = 1
	while True:
		rn += 1
		#print(rn)
		s = len(g[0])
		e, cp = process_elves2(e, cp)
		if e == None:
			return rn
			break
	return rn + 1

def all_elves(g):
	o = []
	for y in range(len(g)):
		for x in range(len(g[0])):
			if g[y][x] != 0:
				o.append((y,x))
	return o

def immobile(g):
	for y in range(len(g)):
		for x in range(len(g[0])):
			if g[y][x] != 0:
				ns = grid.grid_neighbors((y, x), None, None, borderless = True)
				if all(g[p[0]][p[1]] == 0 for p in ns):
					pass
				else:
					return False
	return True


def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
