#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 157)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 70)
		pass

def parse_input(i):
	x = []
	for line in i:
		x.append(line.strip())
	return x

def part1(i):
	s = 0
	for line in i:
		s += get_shared(line)
	return s

def get_shared(sack):
	l = len(sack)
	half = l // 2
	c1 = Counter(sack[:half])
	c2 = Counter(sack[half:])
	p = ""
	for i in c1.keys():
		if c2[i] > 0:
			p = i
			break
	return prio(p)

def prio(p):
	asc = ord(p)
	if asc >= 97:
		return asc - 96
	else:
		return (asc - 64) + 26

def shared2(sacks):
	c1 = Counter(sacks[0])
	c2 = Counter(sacks[1])
	c3 = Counter(sacks[2])
	c4 = []
	for i in c1.keys():
		if c2[i] > 0:
			c4.append(i)
	for i in c4:
		if c3[i] > 0:
			return prio(i)


def part2(i):
	s = 0
	for idx in range(len(i) // 3):
		s += shared2(i[3 * idx:(3 * idx) + 3])
	return s

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
