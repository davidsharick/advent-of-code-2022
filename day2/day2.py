#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """A Y
B X
C Z"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 15)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 12)
		pass

def parse_input(i):
	x = []
	for line in i:
		x.append(line.strip())
	return x

def part1(i):
	d3 = {"A X": 3 + 1, "A Y": 6 + 2, "A Z": 0 + 3, "B X": 0 + 1, "B Y": 3 + 2, "B Z": 6 + 3, "C X": 6 + 1, "C Y": 0 + 2, "C Z": 3 + 3}
	t = 0
	for line in i:
		t += d3[line]
	return t

def part2(i):
	d3 = {"A X": 0 + 3, "A Y": 3 + 1, "A Z": 6 + 2, "B X": 0 + 1, "B Y": 3 + 2, "B Z": 6 + 3, "C X": 0 + 2, "C Y": 3 + 3, "C Z": 6 + 1}
	t = 0
	for line in i:
		t += d3[line]
	return t

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
