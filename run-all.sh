#!/usr/bin/bash


for ((i = 1 ; i <= $1 ; i++)); do
    echo "Day $i:"
    ./day$i/day$i.py day$i/day$i-input.txt $2
done