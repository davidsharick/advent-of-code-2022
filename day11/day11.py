#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 10605)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 2713310158)
		pass

def parse_input(i):
	monkeys = []
	cm = 0
	for line in i:
		if "Monkey" in line:
			cm = int(line.strip().split()[1].strip(":"))
		elif "Starting" in line:
			i = []
			for token in line.strip().split()[2:]:
				i.append(int(token.strip(",")))
		elif "Operation" in line:
			op = []
			tokens = line.strip().split()[3:]
			op.append(tokens[0])
			op.append(tokens[1])
			op.append(tokens[2])
		elif "Test" in line:
			div = int(line.strip().split()[3])
		elif "If true" in line:
			true_case = int(line.strip().split()[-1])
		elif "If false" in line:
			false_case = int(line.strip().split()[-1])
		else:
			monkeys.append([0, i, op, div, true_case, false_case])
	monkeys.append([0, i, op, div, true_case, false_case])
	return monkeys

def part1(i):
	monkeys = i
	for n in range(20):
		monkeys = run_round(monkeys)
	monkeys.sort(key = lambda m: m[0])
	return monkeys[-1][0] * monkeys[-2][0]

def run_round(monkeys):
	for i in range(len(monkeys)):
		monkey = monkeys[i]
		for j in range(len(monkey[1])):
			item = monkey[1][0]
			wl = run_op(monkey[2], item)
			wl = wl // 3
			if wl % monkey[3] == 0:
				to_throw = monkey[4]
				monkeys[to_throw][1].append(wl)
			else:
				to_throw = monkey[5]
				monkeys[to_throw][1].append(wl)
			del monkey[1][0]
			monkey[0] += 1
	return monkeys

def run_op(op, item):
	if op[1] == "+":
		try:
			return item + int(op[2])
		except:
			return item + item
	else:
		try:
			return item * int(op[2])
		except:
			return item * item

def run_round2(monkeys, prod):
	for i in range(len(monkeys)):
		monkey = monkeys[i]
		for j in range(len(monkey[1])):
			item = monkey[1][0]
			wl = run_op2(monkey[2], item, prod)
			if wl % monkey[3] == 0:
				to_throw = monkey[4]
				monkeys[to_throw][1].append(wl)
			else:
				to_throw = monkey[5]
				monkeys[to_throw][1].append(wl)
			del monkey[1][0]
			monkey[0] += 1
	return monkeys

def run_op2(op, item, mod):
	if op[1] == "+":
		try:
			return (item + int(op[2])) % mod
		except:
			return (item + item) % mod
	else:
		try:
			return (item * int(op[2])) % mod
		except:
			return (item * item) % mod

def part2(i):
	monkeys = i
	prod = 1
	for monkey in monkeys:
		prod *= monkey[3]
	for n in range(10000):
		monkeys = run_round2(monkeys, prod)
	monkeys.sort(key = lambda m: m[0])
	return monkeys[-1][0] * monkeys[-2][0]

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
