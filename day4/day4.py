#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 2)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 4)
		pass

def parse_input(i):
	pairs = []
	for line in i:
		prs = line.strip().split(",")
		prz = list(p.split("-") for p in prs)
		pairs.append(prz)
	return pairs

def part1(i):
	t = 0
	for line in i:
		p1 = line[0]
		p2 = line[1]
		if int(p1[0]) >= int(p2[0]) and int(p1[1]) <= int(p2[1]):
			t += 1
		elif int(p2[0]) >= int(p1[0]) and int(p2[1]) <= int(p1[1]):
			t += 1
	return t

def part2(i):
	t = 0
	for line in i:
		p1 = line[0]
		p2 = line[1]
		if (int(p1[1]) >= int(p2[0]) and int(p1[0]) <= int(p2[1])) or (int(p2[0]) <= int(p1[1]) and int(p2[1]) >= int(p1[0])):
			t += 1
	return t

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
