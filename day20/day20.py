#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """1
2
-3
3
-2
0
4"""
		self.test = parse_input(self.testinput.split("\n"))
		self.testinput2 = """0
		10
		-5"""
		self.test2 = parse_input(self.testinput2.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 3)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 1623178306)
		pass
	
	def test_mix(self):
		self.assertEqual(mix_once(decorate(self.test2)), [(0,0), (-5, 2), (10,1)])

def parse_input(i):
	o = []
	ma = 0
	mi = 0
	for line in i:
		o.append(int(line.strip()))
		ma = max(ma, int(line.strip()))
		mi = min(mi, int(line.strip()))
	return o

def part1(i):
	i2 = decorate(i)
	o = mix_once(i2)
	si = 0
	for idx in range(len(o)):
		if o[idx][0] == 0:
			si = idx
			break
	indices = []
	for v in o: 
		indices.append(v[1])
	assert len(set(indices)) == len(indices)
	id1 = (si + 1000) % len(o)
	id2 = (si + 2000) % len(o)
	id3 = (si + 3000) % len(o)
	return o[id1][0] + o[id2][0] + o[id3][0]

def decorate(i):
	q = []
	for idx, val in enumerate(i):
		q.append((val, idx))
	return q

def mix_once(l):
	l2 = l.copy()
	need = len(l)
	for i in range(need):
		startplace = 0
		for idx, v in enumerate(l2):
			if v[1] == i:
				startplace = idx
				break
		between_index = 0 if startplace == len(l2) - 1 else startplace
		new_idx = (between_index + l2[startplace][0]) % (len(l2) - 1)
		if new_idx >= len(l2):
			new_idx -= 1
			new_idx %= len(l2)
		elif new_idx < 0:
			new_idx -= 1
			new_idx %= len(l2)
		val = l2[startplace]
		if new_idx == 0:
			l2.append(val)
		elif new_idx >= startplace:
			l2.insert(new_idx + 1, val)
		else:
			l2.insert(new_idx, val)
		if new_idx >= startplace or new_idx == 0:
			del l2[startplace]
		else:
			del l2[startplace + 1]
	return l2

def part2(i):
	i = list(x * 811589153 for x in i)
	i2 = decorate(i)
	for _ in range(10):
		i2 = mix_once(i2)
	o = i2
	si = 0
	for idx in range(len(i2)):
		if o[idx][0] == 0:
			si = idx
			break
	indices = []
	for v in i2:
		indices.append(v[1])
	assert len(set(indices)) == len(indices)
	id1 = (si + 1000) % len(o)
	id2 = (si + 2000) % len(o)
	id3 = (si + 3000) % len(o)
	return o[id1][0] + o[id2][0] + o[id3][0]

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
