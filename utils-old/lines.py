#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest

@lru_cache(None)
def lines_from_pos(dimension, position, ranges, direction):
    if len(position) != dimension:
        raise ValueError(f"Length of position ({len(position)} should equal dimension {dimension}")
    if len(ranges) != dimension:
        raise ValueError(f"Length of position ({len(position)} should equal dimension {dimension}")
    for r in ranges:
        if len(r) != 2:
            raise ValueError(f"Length of range {r} should be 2")
    out = []
    while True:
        new_pos = tuple(position[i] + direction[i] for i in range(dimension))
        for i in range(dimension):
            if new_pos[i] < ranges[i][0] or new_pos[i] > ranges[i][1]:
                return out
            else:
                pass
        out.append(new_pos)
        position = new_pos

@lru_cache(None)
def lines_on_grid(position, grid_size, direction):
    y_range = (0, grid_size[0] - 1)
    x_range = (0, grid_size[1] - 1)
    if direction == "upleft":
        d = (-1, -1)
    elif direction == "up":
        d = (-1, 0)
    elif direction == "upright":
        d = (-1, 1)
    elif direction == "left":
        d = (0, -1)
    elif direction == "right":
        d = (0, 1)
    elif direction == "downleft":
        d = (1, -1)
    elif direction == "down":
        d = (1, 0)
    elif direction == "downright":
        d = (1, 1)
    else:
        raise ValueError(f"Invalid direction {direction}; make sure diagonals are in the form e.g. \"upright\", with no spacing")
    return lines_from_pos(2, position, (y_range, x_range), d)
