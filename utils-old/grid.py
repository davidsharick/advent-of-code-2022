#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest

@lru_cache(None)
def grid_neighbors(position, y_range, x_range, wrap = False, borderless = False, corners = True):
	out = []
	up = position[0] - 1
	down = position[0] + 1
	left = position[1] - 1
	right = position[1] + 1
	if borderless:
		return [(up, left),
				(up, position[1]),
				(up, right),
				(position[0], left),
				(position[0], right),
				(down, left),
				(down, position[1]),
				(down, right)]
	if wrap: # not really implemented due to not having enough of a concrete idea of the details
		if up < y_range[0]:
			up = y_range[1]
		if down > y_range[1]:
			down = y_range[0]
		if left < x_range[0]:
			left = x_range[1]
		if right > x_range[1]:
			right = x_range[0]
	else:
		if up < y_range[0]:
			up = None
		if down > y_range[1]:
			down = None
		if left < x_range[0]:
			left = None
		if right > x_range[1]:
			right = None
	if up is not None:
		if left is not None:
			if corners:
				out.append((up, left))
		out.append((up, position[1]))
		if right is not None:
			if corners:
				out.append((up, right))
	if left is not None:
		out.append((position[0], left))
	if right is not None:
		out.append((position[0], right))
	if down is not None:
		if left is not None:
			if corners:
				out.append((down, left))
		out.append((down, position[1]))
		if right is not None:
			if corners:
				out.append((down, right))
	return out

#print(grid_neighbors((1, 1), (0, 2), (0, 1)))

def grid_dijkstra(grid, start, path_function, corners = True, wrap = False, borderless = False):
	nodes = []
	adj = {}
	for y in range(len(grid)):
		for x in range(len(grid[y])):
			pos = (y, x)
			nodes.append(pos)
			neighbors = grid_neighbors(grid, (0, len(grid) - 1), (0, len(grid[y ] - 1)), wrap = wrap, borderless = borderless, corners = corners)
			for ni in neighbors:
				weight = path_function(pos, ni)
				if weight >= 0:
					try:
						adj[n].append([ni, weight])
					except:
						adj[n] = [[ni, weight]]
	dist = {start: 0}
	prev = {}
	queue = [start]
	for n in nodes:
		if n != start:
			dist[v] = math.inf
			prev[v] = None
			queue.append(n)
	
	while len(queue) > 0:
		queue = sorted(queue, key = lambda v: dist[v])
		smallest = queue[0]
		queue = queue[1:]
		for n in adj[smallest]:
			new_dist = dist[smallest] + n[1]
			if new_dist < dist[n[0]]:
				dist[n[0]] = new_dist
				prev[n[0]] = smallest
	return dist, prev