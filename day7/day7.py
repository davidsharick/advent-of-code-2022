#!/usr/bin/env python3

from collections import defaultdict, Counter
from functools import lru_cache
import itertools
import math
import string
import sys
import timeit
import unittest

class Tests(unittest.TestCase):
	def setUp(self):
		self.testinput = """$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k"""
		self.test = parse_input(self.testinput.split("\n"))
	
	def test_part1(self):
		self.assertEqual(part1(self.test), 95437)
		pass
		
	def test_part2(self):
		self.assertEqual(part2(self.test), 24933642)
		pass

def parse_input(i):
	all_files = []
	all_dirs = []
	filesizes = {}
	curr_dir = ""
	for line in i:
		if line.strip() == "$ cd /":
			all_files = []
		elif line.strip().startswith("$ cd"):
			if line.strip().split()[2] == "..":
				curr_dir = curr_dir[:curr_dir.rfind("/")]
			else:
				curr_dir = curr_dir + "/" + line.strip().split()[2]
		elif line.strip().split()[0] == "dir":
			all_files.append(curr_dir + "/" + line.strip().split()[1])
			all_dirs.append(curr_dir + "/" + line.strip().split()[1])
		elif line.strip() == "$ ls":
			pass
		else:
			f = curr_dir + "/" + line.strip().split()[1]
			all_files.append(f)
			filesizes[f] = line.strip().split()[0]
	return (all_files, all_dirs, filesizes)

def part1(i):
	all_files, all_dirs, filesizes = i[0], i[1], i[2]
	ts = 0
	for dir in all_dirs:
		s = 0
		for file in filesizes.keys():
			if file.startswith(dir):
				s += int(filesizes[file])
		if s <= 100000:
			ts += s
	s = 0
	for file in filesizes.keys():
		s += int(filesizes[file])
	if s <= 100000:
		ts += s
	return ts

def part2(i):
	all_files, all_dirs, filesizes = i[0], i[1], i[2]
	ts = 0
	smallest_size = 0
	unused = 70000000
	for file in filesizes.keys():
		smallest_size += int(filesizes[file])
	unused -= smallest_size
	need = 30000000 - unused
	for dir in all_dirs:
		s = 0
		for file in filesizes.keys():
			if file.startswith(dir):
				s += int(filesizes[file])
		if s <= smallest_size and s >= need:
			smallest_size = s
	return smallest_size

def main():
	t0 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part1(i))
	t1 = timeit.default_timer()
	with open(sys.argv[1], "r") as f:
		i = parse_input(f.readlines())
	print(part2(i))
	t2 = timeit.default_timer()
	if len(sys.argv) > 2:
		print(f"Part 1: {t1 - t0} seconds")
		print(f"Part 2: {t2 - t1} seconds")

if __name__ == '__main__':
	if len(sys.argv) < 2:
		unittest.main()
	else:
		main()
